var express = require('express');
var passportFacebook = require('../auth/facebook');
var passportGoogle = require('../auth/google');
var router = express.Router();

router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/home', function(req, res, next) {
  res.render('home', { title: 'Express' });
});

router.get('/auth/facebook', passportFacebook.authenticate('facebook', { scope: ['email', 'public_profile', 'user_friends']}));

router.get('/auth/facebook/callback',
  passportFacebook.authenticate('facebook', { successRedirect: '/home',
                                      failureRedirect: '/error' })
  );

router.get('/auth/google', passportGoogle.authenticate('google', { scope: 
  	[ 'https://www.googleapis.com/auth/plus.login',
  		'https://www.googleapis.com/auth/plus.profile.emails.read',
      'https://www.googleapis.com/auth/plus.me' ]}));

router.get('/auth/google/callback',
  passportGoogle.authenticate('google', { successRedirect: '/home',
                                      failureRedirect: '/error' })
  );

module.exports = router;
