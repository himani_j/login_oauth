var mongoose = require('mongoose');
var Schema = mongoose.Schema,
	ObjectId = Schema.ObjectId;


// create User Schema
var User = new Schema({
  fbName: String,
  googleName: String,
  fbID: String,
  googleID: String,
  email: { type: String, unique: true},
  password: String,
  fbFriends: [{ type : ObjectId, ref: 'User' }],
  googleFriends: [{ type : ObjectId, ref: 'User' }],
  fbImageUrl: String,
  googleImageUrl: String
});


module.exports = mongoose.model('users', User);
