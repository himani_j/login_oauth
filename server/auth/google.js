var passport = require('passport');
var GoogleStrategy = require('passport-google-oauth2').Strategy;

var User = require('../models/user');
var config = require('../_config');
var init = require('./init');
var mongoose = require('mongoose');
var mongodb = require('mongodb');
var MongoClient = mongodb.MongoClient;
var url = 'mongodb://localhost/passport-social-auth-2';
var https = require("https");


passport.use(new GoogleStrategy({
  clientID: config.google.clientID,
  clientSecret: config.google.clientSecret,
  callbackURL: config.google.callbackURL,
  passReqToCallback   : true
},
  // linkedin sends back the tokens and progile info
  function(request, accessToken, refreshToken, profile, done) {
    console.log(profile);

    var searchQuery = {
      email: profile.email
    };


    var updates = {
      email: profile.email,
      fbID: "",
      fbImageUrl: "",
      fbName: "",
      fbFriends: [],
      googleID: profile.id,
      googleImageUrl: profile._json.picture,
      googleName: profile.displayName,
      googleFriends: []
    };

     var updates2 = {$set: {
      googleID: profile.id,
      googleImageUrl: profile._json.picture,
      googleName: profile.displayName
    }}

    var option = {
      upsert: true,
      new: true
    };
   
    // Use connect method to connect to the Server
    MongoClient.connect(url, function (err, db) {
      if (err) 
      {
        console.log('Unable to connect to the mongoDB server. Error:', err);
      } else 
      {
        var users = db.collection('users');
        var options = {
          host: "www.googleapis.com",
          path: "/plus/v1/people/me/people/visible?access_token=" + accessToken,
          method: 'GET'
        };


        users.find({"email": profile.email}).toArray(
            function(err, docs){
              if(docs.length === 0){
                findAndModify(updates, 1);
              }else{
                console.log(docs[0])
                findAndUpdate(updates2,docs[0],docs[0]._id,1)
              }
            }
        )

        // findAndModify(updates, 1);

        function findAndModify(updates, callback){
          users.findAndModify(
            searchQuery,
            [],
            updates,
            option,
            function(err, user) {
              if (err){
                // console.log("if")
                // console.log(profile._json['email'])
                // console.warn(err.message); 
                return done(err); // returns error if no matching object found
              }else{
                // console.log("else")
                // console.log(user['value'])
                // console.log(user.value)
                if(callback === 1){
                  update_friends(user.value._id, user);
                }else{
                  console.log("not 1")
                  return done(null, user);
                }
              }
            }
          );
        }

        function findAndUpdate(updates,user, user_id, callback){
          users.update(
            searchQuery,
            updates,
            {upsert: false},
            function(err, result) {
              if (err){
                console.log("if")
                console.log(profile._json['email'])
                console.warn(err.message); 
                return done(err); // returns error if no matching object found
              }else{
                // console.log("else")
                // console.log(user)
                // console.log(user.value)

                // if(callback === 1){
                  update_friends(user_id, user);
                // }else{
                //   console.log("not 1")
                //   return done(null, user);
                // }
              }
            }
          );
        }

        function update_friends(user_id,user){
          var req = https.get(options, function(res) {
            var output = '';
            console.log(options.host + ':' + res.statusCode);
            res.setEncoding('utf8');

            res.on('data', function (chunk) {
              output += chunk;
            }).on('end', function() {
              var obj = JSON.parse(output);
              updates_friends = []
              if(obj.items.length === 0){
                return done(null, user);
              }
              obj.items.forEach(function(element, index, array) {
                // console.log(element["id"]);
                users.findAndModify(
                  { googleID: element["id"] },
                  [],
                  { $addToSet: { googleFriends: user_id } },
                  {upsert: false},
                  function(err, object) {
                    if (err){
                      console.warn(err.message);  // returns error if no matching object found
                    }else{
                      console.log("hey")
                      if(object.value != null){
                        updates_friends.push(object.value._id)
                        console.log(updates_friends)
                      }
                      if (array.length === index+1){
                        console.log("if")
                        console.log(updates_friends)
                        findAndModify({ $set: { googleFriends: updates_friends } }, 2);
                      }
                    }
                  }
                );
              });
              
            });
          });
          req.on('error', function(e) {
            console.log('ERROR: ' + e.message);
          });
        }
      }
        // });

      
      // findAndModify();
        // db.close();
    });



    // update the user if s/he exists or add a new user
    // function findAndModify(){
    //   User.findOneAndUpdate(searchQuery, updates, options, function(err, user) {
    //     if(err) {
    //       return done(err);
    //     } else {
    //       return done(null, user);
    //     }
    //   });
    // }
  }

));




// serialize user into the session
init();


module.exports = passport;