var passport = require('passport');
var FacebookStrategy = require('passport-facebook').Strategy;

var User = require('../models/user');
var config = require('../_config');
var init = require('./init');
var mongoose = require('mongoose');
var mongodb = require('mongodb');
var MongoClient = mongodb.MongoClient;
var url = 'mongodb://localhost/passport-social-auth-2';
var https = require("https");


passport.use(new FacebookStrategy({
  clientID: config.facebook.clientID,
  clientSecret: config.facebook.clientSecret,
  callbackURL: config.facebook.callbackURL,
  profileFields: ['id', 'displayName', 'email','friends','picture.type(large)']
},
  // linkedin sends back the tokens and progile info
  function(token, tokenSecret, profile, done) {
    console.log(profile);

    var searchQuery = {
      email: profile._json['email']
    };

    function listOfFriendIds(){
      return profile._json["friends"]["data"].map(function(item,index){
        return item["id"];
      });
    };


    var options = {
      upsert: true,
      new: true
    };

    var updates = {
      email: profile._json['email'],
      fbID: profile.id,
      fbImageUrl: profile.photos[0]['value'],
      fbName: profile.displayName,
      fbFriends: [],
      googleID: "",
      googleImageUrl: "",
      googleName: "",
      googleFriends: []
    };

    var updates2 = {$set: {
      fbID: profile.id,
      fbImageUrl: profile.photos[0]['value'],
      fbName: profile.displayName
    }}
    // Use connect method to connect to the Server
    MongoClient.connect(url, function (err, db) {
      if (err) {
        console.log('Unable to connect to the mongoDB server. Error:', err);
      } else {
        //HURRAY!! We are connected. :)
        var users = db.collection('users');
        console.log('Connection established to', url);
       
        users.find({"email": profile._json['email']}).toArray(
          function(err, docs){
            if(docs.length === 0){
              findAndModify(updates, 1);
            }else{
              console.log(docs[0])
              findAndUpdate(updates2,docs[0],docs[0]._id,1)
            }
          }
        )

        // findAndModify(updates, 1);

        function findAndModify(updates, callback){
          users.findAndModify(
            searchQuery,
            [],
            updates,
            options,
            function(err, user) {
              if (err){
                // console.log("if")
                // console.log(profile._json['email'])
                // console.warn(err.message); 
                return done(err); // returns error if no matching object found
              }else{
                // console.log("else")
                // console.log(user['value'])
                // console.log(user.value)
                if(callback === 1){
                  update_friends(user.value._id, user);
                }else{
                  console.log("not 1")
                  return done(null, user);
                }
              }
            }
          );
        }

        function findAndUpdate(updates,user, user_id, callback){
          users.update(
            searchQuery,
            updates,
            {upsert: false},
            function(err, result) {
              if (err){
                console.log("if")
                console.log(profile._json['email'])
                console.warn(err.message); 
                return done(err); // returns error if no matching object found
              }else{
                // console.log("else")
                // console.log(user)
                // console.log(user.value)

                // if(callback === 1){
                  update_friends(user_id, user);
                // }else{
                //   console.log("not 1")
                //   return done(null, user);
                // }
              }
            }
          );
        }
        // console.log(listOfFriendIds());
        function update_friends(user_id,user){
          var objects = users.find({"fbID": { $in: listOfFriendIds() }}).toArray(
            function(err, docs){
              
              if (docs.length === 0) {
                return done(null, user);
              }
              update_friends = []
              docs.forEach(function(element, index, array) {
                update_friends.push(element._id)
                users.update(
                  { fbID: element.fbID },
                  { $addToSet: { fbFriends: user_id } },
                  false,
                  true
                );
                if (index === array.length - 1) {
                  findAndModify({ $set: { fbFriends: update_friends } }, 2);
                }
              });
            }
          );
        }
        //Close connection
        // db.close();
    }});

    // console.log("friends"+ friendsIds);

    


    // update the user if s/he exists or add a new user
    // function findAndModify(){
    //   User.findOneAndUpdate(searchQuery, updates, options, function(err, user) {
    //     if(err) {
    //       return done(err);
    //     } else {
    //       return done(null, user);
    //     }
    //   });
    // }
  }

));

// serialize user into the session
init();


module.exports = passport;