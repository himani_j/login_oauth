var passport = require('passport');
var User = require('../models/user');
var mongoose = require('mongoose');


module.exports = function() {

  passport.serializeUser(function(user, done) {
    done(null, user);
  });

  passport.deserializeUser(function(user, done) {
    done(null, user);
  });

};
